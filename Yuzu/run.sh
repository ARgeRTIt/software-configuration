#! /usr/bin/sh

value=`gsettings get org.gnome.desktop.session idle-delay | cut -d' ' -f2`
gsettings set org.gnome.desktop.session idle-delay 0
#env QT_QPA_PLATFORM=wayland ./yuzu.AppImage
unshare -m -n -r ./yuzu.AppImage
gsettings set org.gnome.desktop.session idle-delay $value
