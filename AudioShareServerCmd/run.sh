#!/bin/sh

# handle ctrl-c: https://superuser.com/questions/1653229/when-pressed-ctrlc-only-break-out-of-current-function-and-not-whole-bash-script
trap '' INT

# command reference: https://bbs.archlinux.org/viewtopic.php?id=284209
device_id=$(pw-cli info "alsa_card.pci-0000_00_1f.3" | head -n 1 | awk '{ print $2 }')
# get current profile: ~pw-cli enum-params <object-id> Profile~
# get all profile: ~pw-cli e <card-id> EnumProfile~ , reference: https://unix.stackexchange.com/questions/753211/how-to-set-audio-device-profile-and-route-with-pipewire-directly
temp=`pw-cli enum-params $device_id EnumProfile`
# use unused profile, so the headset will not sound together
speaker_profile_id=`echo "$temp" | grep -B 2 "output:iec958-stereo+input:analog-stereo" | head -n 1 | awk '{ print $2 }'`
headset_profile_id=`echo "$temp" | grep -B 2 "output:analog-stereo+input:analog-stereo" | head -n 1 | awk '{ print $2 }'`
# profile_id may not permanent ? (only met once)
#headset_profile_id=1
#speaker_profile_id=3

wpctl set-profile $device_id $speaker_profile_id
sleep 1
# if don't execute this command, the max volume is limitted under the volume that the (same device)? last used
wpctl set-volume @DEFAULT_AUDIO_SINK@ 0.3

(trap - INT; ~/Softwares/audio-share-server-cmd/bin/as-cmd -b)

wpctl set-profile $device_id $headset_profile_id
sleep 1
wpctl set-volume @DEFAULT_AUDIO_SINK@ 0.2
