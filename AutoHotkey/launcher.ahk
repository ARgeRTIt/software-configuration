
; ------------------------ 启动器 ------------------------

class Item {
    __New(Keyword, Name, Path, Parameter := "") {
        this.Keyword := Keyword
        this.Name := Name
        this.Path := Path
        this.Parameter := Parameter
    }
}

g_CommandArray := [
    Item("cmd", "CMD", "Cmd.exe"),
    Item("calc", "Calc", "Calc.exe"),
    Item("controlpanel", "Control Panel", "control.exe"),
    Item("explorer", "Explorer", "explorer.exe", "D:\"),
    Item("firewall", "Firewall", "WF.msc"),
    Item("note", "notepad", "notepad.exe"),
    Item("mspaint", "mspaint", "mspaint.exe"),
    Item("shutdown", "Shutdown", "SlideToShutDown.exe"),
    Item("snippingtool", "SnippingTool", "SnippingTool.exe"),
]
g_TempListViewKeywords := []
g_MyGuiWinTitle := "ahk_class AutoHotkeyGUI"

GuiEscape(GuiObj) {
    global g_TempListViewKeywords
    if (0 < g_TempListViewKeywords.Length)
        g_TempListViewKeywords := []
    GuiObj.Destroy()
}

MyGuiLoseFocus(GuiCtrlObj, _) {
    GuiEscape(GuiCtrlObj.Gui)
}

ListViewSelectUpDown(ListViewControlObj, Step) {
    LVCount := ListViewControlObj.GetCount()
    if (LVCount <= 0)
        return
    RowNumber := ListViewControlObj.GetNext(0) + Step
    if (Step >= 0)
        RowNumber := RowNumber > LVCount ? LVCount : RowNumber
    else
        RowNumber := RowNumber <= 0 ? 1 : RowNumber
    ListViewControlObj.Modify(RowNumber, "Select Vis")
}

ShowGui(FontSize, GuiControlWidth, GetGuiPosFunc, EditChangeFunc, ClickFunc, EditDefaultValue:="", ListViewDefaultValues:="") {
    ; config
    BackgroundColor := "505050"
    FontColor := "White"
    ListViewNextSelectKey := "^n"
    ListViewPrevSelectKey := "^p"
    ListViewRowCount := 3

    PosArray := GetGuiPosFunc()

    ; implemention

    MyGui := Gui("AlwaysOnTop -Caption -SysMenu ToolWindow")
    MyGui.MarginX := 0
    MyGui.MarginY := 0
    MyGui.SetFont(Format("S{1} bold C{2}", FontSize, FontColor))
    MyGui.OnEvent("Escape", GuiEscape) ; Esc 关闭窗口

    MyEdit := MyGui.Add("Edit", Format("W{1} R1 Background{2}", GuiControlWidth, BackgroundColor))
    if (EditDefaultValue)
        MyEdit.Value := EditDefaultValue
    MyEdit.OnEvent("LoseFocus", MyGuiLoseFocus)

    ListViewOptions := Format("W{1} -Hdr -E0x200 -Multi NoSortHdr NoSort ReadOnly R{2} Background{3}",
                            GuiControlWidth,
                            ListViewRowCount,
                            BackgroundColor)
    LV := MyGui.Add("ListView", ListViewOptions, ["name"])
    if (ListViewDefaultValues) {
        for Value in ListViewDefaultValues {
            LV.Add(, Value)
        }
    }
    LV.Modify(1, "Select")
    LV.ModifyCol(1, GuiControlWidth - 20)
    LV.OnEvent("Click", ClickFunc)
    LV.OnEvent("LoseFocus", MyGuiLoseFocus)

    EditChange(GuiCtrlObj, _) {
        EditChangeFunc(GuiCtrlObj.value, LV)
    }
    MyEdit.OnEvent("Change", EditChange)

    if (PosArray)
        MyGui.show(Format("AutoSize X{1} Y{2}", PosArray[1], PosArray[2]))
    else
        MyGui.show(Format("AutoSize Center"))

    MyGuiHotKey(ThisHotkey) {
        Switch ThisHotkey {
            Case ListViewNextSelectKey:
                ListViewSelectUpDown(LV, 1)
            Case ListViewPrevSelectKey:
                ListViewSelectUpDown(LV, -1)
            Case "Enter":
                ClickFunc(LV, LV.GetNext(0))
            Case "NumpadEnter":
                ClickFunc(LV, LV.GetNext(0))
            Case "Up":
                ListViewSelectUpDown(LV, -1)
            Case "Down":
                ListViewSelectUpDown(LV, 1)
        }
    }

    HotIfWinActive g_MyGuiWinTitle
    Hotkey ListViewNextSelectKey, MyGuiHotKey
    Hotkey ListViewPrevSelectKey, MyGuiHotKey
    Hotkey "Enter", MyGuiHotKey
    Hotkey "NumpadEnter", MyGuiHotKey
    Hotkey "Up", MyGuiHotKey
    Hotkey "Down", MyGuiHotKey
}

MainLauncher_GetGuiPosFunc() {
    return ""
}
MainLauncher_EditChangeFunc(Content, ListViewControlObj) {
    global g_TempListViewKeywords
    g_TempListViewKeywords := []
    ListViewControlObj.Delete()

    if ("" == Content)
        return
    for Item in g_CommandArray {
        Keyword := Item.Keyword
        if (1 == InStr(Keyword, Content, True)) {
            g_TempListViewKeywords.Push(Keyword)
            ListViewControlObj.Add(, Item.Name)
        }
    }
    ListViewControlObj.Modify(1, "Select")
}
MainLauncher_ClickFunc(GuiCtrlObj, Info) {
    if 0 >= Info
        return

    Keyword := g_TempListViewKeywords[Info]
    for TempItem in g_CommandArray {
        if (Keyword == TempItem.Keyword) {
            Item := TempItem
            break
        }
    }
    GuiEscape(GuiCtrlObj.Gui)

    Command := Item.Path
    WorkingDir := A_Desktop
    if (InStr(Command, ":") > 1)
        SplitPath Command, , &WorkingDir

    if (Item.Parameter != "")
        Run Format("{1} {2}", Command, Item.Parameter), WorkingDir
    else
        Run Command, WorkingDir
}

^+z:: {
    ShowGui(25, 500, MainLauncher_GetGuiPosFunc, MainLauncher_EditChangeFunc, MainLauncher_ClickFunc)
}

; ------------------------ 资源管理器 即时搜索 ------------------------

GetFileNamesFromPath(Path, Keyword) {
    TempResult := []
    DirectoryCount := 1
    Loop Files, Path . "*", "DF" {
    ; A_LoopFileAttrib
        if ((!RegExMatch(A_LoopFileAttrib, "[SH]")) && InStr(A_LoopFileName, Keyword)) {
            if (InStr(A_LoopFileAttrib, "D"))
                TempResult.InsertAt(DirectoryCount++, A_LoopFileName)
            else
                TempResult.Push(A_LoopFileName)
        }
    }
    return TempResult
}

#HotIf WinActive("ahk_class CabinetWClass")
1::
2::
3::
4::
5::
6::
7::
8::
9::
0::
q::
w::
e::
r::
t::
y::
u::
i::
o::
p::
a::
s::
d::
f::
g::
h::
j::
k::
l::
z::
x::
c::
v::
b::
n::
m:: {
    Hwnd := WinGetID("A")
    FocusedControl := ControlGetFocus(Hwnd)
    ; 判断是否在地址栏输入中，或使用右键菜单
    if ((FocusedControl && RegExMatch(ControlGetClassNN(FocusedControl), "^Edit"))
        or WinExist("ahk_class #32768")) {
        Send A_ThisHotkey
        return
    }

    TempText := ControlGetText("ToolbarWindow323", Hwnd)
    TempText := SubStr(TempText, InStr(TempText, ":") + 1)
    Path := Trim(TempText)
    if (SubStr(Path, -1) != "\")
        Path .= "\"
    TempResult := GetFileNamesFromPath(Path, A_ThisHotkey)
    if (0 >= TempResult.Length)
        return

    GuiControlWidth := 400
    GetGuiPosFunc() {
        WinGetPos &ParentX, &ParentY, &ParentWidth, &ParentHeight, Hwnd
        MyGuiX := ParentX + ParentWidth - GuiControlWidth - 120
        MyGuiY := ParentY + ParentHeight // 2
        return [MyGuiX, MyGuiY]
    }
    EditChangeFunc(Content, ListViewControlObj) {
        ListViewControlObj.Delete()

        if ("" == Content)
            return
        TempResult := GetFileNamesFromPath(Path, Content)
        if (0 >= TempResult.Length)
            return
        for Name in TempResult
            ListViewControlObj.Add(, Name)
        ListViewControlObj.Modify(1, "Select")
    }
    ClickFunc(ListViewControlObj, Info) {
        Text := ListViewControlObj.GetText(Info)
        ControlFocus("ToolbarWindow323", Hwnd)
        Send "{Enter}"
        SendText Format("{1}{2}", Path, Text)
        Send "{Enter}"
    }
    ShowGui(20, GuiControlWidth, GetGuiPosFunc, EditChangeFunc, ClickFunc, A_ThisHotkey, TempResult)
    Send "{Right}" ; 设置 value 再 show 会选中文本
}
#HotIf
