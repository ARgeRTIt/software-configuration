# &#30446;&#24405;

1.  [功能](#orge51b5bf)
2.  [启动器截图](#orge9dbfa6)



<a id="orge51b5bf"></a>

# 功能

-   绑定一些快捷键
-   简易的快捷启动器
-   模拟鼠标


<a id="orge9dbfa6"></a>

# 启动器截图

<img src="readme-images/1.png" width="300">

<img src="readme-images/2.png" width="300">

<img src="readme-images/3.png" width="300">
