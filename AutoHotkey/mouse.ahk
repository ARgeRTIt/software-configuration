
MouseMove 0, 0, 100, "R" ; add reason: 开机后鼠标不显示

CoordMode "Mouse", "Screen"
g_MouseMoveXSpeedMulti := 1
g_MouseMoveYSpeedMulti := 1

SetGMouseMoveXSpeedMulti(Value, AddFlag) {
    global g_MouseMoveXSpeedMulti
    if (AddFlag)
        g_MouseMoveXSpeedMulti += Value
    else
        g_MouseMoveXSpeedMulti := Value
}

SetGMouseMoveYSpeedMulti(Value, AddFlag) {
    global g_MouseMoveYSpeedMulti
    if (AddFlag)
        g_MouseMoveYSpeedMulti += Value
    else
        g_MouseMoveYSpeedMulti := Value
}

MyMouseMove(XSpeed, YSpeed) {
    MouseGetPos &XPos, &YPos
    if (0 != XSpeed) {
        XPos += XSpeed * g_MouseMoveXSpeedMulti
        SetGMouseMoveXSpeedMulti(2, true)
    }
    if (0 != YSpeed) {
        YPos += YSpeed * g_MouseMoveYSpeedMulti
        SetGMouseMoveYSpeedMulti(2, true)
    }
    DllCall("SetCursorPos", "int", xpos, "int", ypos)
}

!h::MyMouseMove(-3, 0)
!h Up::SetGMouseMoveXSpeedMulti(1, false)
!l::MyMouseMove(3, 0)
!l Up::SetGMouseMoveXSpeedMulti(1, false)
!j::MyMouseMove(0, 3)
!j Up::SetGMouseMoveYSpeedMulti(1, false)
!k::MyMouseMove(0, -3)
!k Up::SetGMouseMoveYSpeedMulti(1, false)
!^j::WheelDown
!^k::WheelUp


g_MouseDownFlag := false
!q:: {
    global g_MouseDownFlag
    if (g_MouseDownFlag)
        return
    g_MouseDownFlag := true
    SendEvent "{Click Down}"
}
!q Up:: {
    global g_MouseDownFlag
    g_MouseDownFlag := false
    SendEvent "{Click Up}"
}
!i::Click "Right"