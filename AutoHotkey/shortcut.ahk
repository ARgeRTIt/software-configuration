^!r::Reload
;^!d::MsgBox WinGetClass("A") ; 显示当前活动窗口的类名
^!d:: { ; 显示光标悬停的窗口类，及控件
    MouseGetPos , , &Id, &Control
    MsgBox Format("ahk_class:`t {1}`nControl:`t {2}", WinGetClass(Id), Control)
}

#Up::Volume_Up
#Down::Volume_Down
#Space::Media_Play_Pause
#Left::Media_Prev
#Right::Media_Next
#c:: A_Clipboard := ""
+CapsLock::CapsLock
CapsLock::Ctrl