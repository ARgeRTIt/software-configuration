#!/bin/sh

readonly TEST_TYPE_SHORT=0
readonly TEST_TYPE_LONG=1

script_directory=$(dirname $(readlink -f "$0"))
#echo $script_directory >&2
data_directory=${script_directory}/Data
if [ ! -d "${data_directory}" ]; then
    mkdir "${data_directory}"
fi
data_path=${data_directory}/check-hard-drive-health.env
if [ -f "${data_path}" ]; then
    source ${data_path}
else
    timestamp_last_check=""
    timestamp_next_test=""
    test_type_next=""
fi

function saveData() {
    printf "timestamp_last_check=%s\ntimestamp_next_test=%s\ntest_type_next=%s" \
	   "${timestamp_last_check}" \
	   "${timestamp_next_test}" \
	   "${test_type_next}" \
	   > "${data_path}"
}

function getCurrentTimestamp() {
    echo $EPOCHSECONDS
}

function getDayIdentify() {
    echo `date --date="@$1" +%Y%m%d`
}

function isTestNeedRun() {
    if [[ ${timestamp_next_test} > $1 ]]; then
	return 1
    fi
    return 0
}

function isHealthChecked() {
    if [[ -z ${timestamp_last_check} ]]; then
	return 1
    fi
    local day_identify_last=`getDayIdentify ${timestamp_last_check}`
    local day_identify_cur=`getDayIdentify $1`
    if [[ ${day_identify_last} != ${day_identify_cur} ]]; then
	return 1
    fi
    return 0
}

function joinByChar() {
    local IFS="$1"
    shift
    echo "$*"
}

function checkHealth() {
    local error=()
    for device in `sudo smartctl --scan | awk '{ print $1 }'`; do
	sudo smartctl -H $device # &> /dev/null
	if [[ $? != 0 ]]; then
	    error[${#error[@]}]=$device
	fi
    done

    if [[ ${#error[@]} == 0 ]]; then
	return 0
    fi	

    notify-send "Smartctl S.M.A.R.T Error" "error device: $(joinByChar , ${error[@]})" --icon=dialog-warning -u critical
    return 1
}

function runTest() {
    local parameter
    if [[ ${TEST_TYPE_SHORT} == ${test_type_next} ]]; then
	parameter="-t short"
    else
	parameter="-t long"
    fi

    local error=()
    for device in `sudo smartctl --scan | awk '{ print $1 }'`; do
	sudo smartctl $parameter $device # &> /dev/null
	if [[ $? != 0 ]]; then
	    error[${#error[@]}]=$device
	fi
    done

    if [[ ${#error[@]} == 0 ]]; then
	return 0
    fi	
    notify-send "Smartctl Test Error" "error device: $(joinByChar , ${error[@]})" --icon=dialog-warning -u critical
    return 1
}

function calcTimestamp() {
    local current_timestamp=$1
    local arr=(${@:2})
    local current_year=`date --date="@${current_timestamp}" +%Y`
    for temp in "${arr[@]}"; do
	local timestamp=`date --date="${current_year}-${temp}" +%s`
	if [ $timestamp -le ${current_timestamp} ]; then
	    continue
	fi
	echo $timestamp
	return
    done
    current_year=$((${current_year} + 1))
    date --date="${current_year}-${arr[0]}" +%s
}

function calcTimestampForNextShortTest() {
    local arr=(
	"06-01T10:00:00"
	"09-01T10:00:00"
	"12-01T10:00:00"
    )
    calcTimestamp $1 ${arr[@]}
}

function calcTimestampForNextLongTest() {
    # same as "calcTimestampForNextShortTest"
    #echo ""

    local arr=(
	"03-01T10:00:00"
    )
    calcTimestamp $1 ${arr[@]}
}

function calcTimestampForNextTest() {
    local timestamp_short=`calcTimestampForNextShortTest $1`
    local timestamp_long=`calcTimestampForNextLongTest $1`
    if [[ -z ${timestamp_long} ]]; then
	echo $timestamp_short
	return $TEST_TYPE_SHORT
    fi
    if [[ ${timestamp_short} < ${timestamp_long} ]]; then
	echo $timestamp_short
	return $TEST_TYPE_SHORT
    fi
    echo $timestamp_long
    return $TEST_TYPE_LONG
}

function setNextTestVariables() {
    timestamp_next_test=`calcTimestampForNextTest $1`
    test_type_next=$?
    #echo $timestamp_next_test $test_type_next >&2
}

function main() {
    echo "======== check hard drive health ========"
    local current_timestamp=`getCurrentTimestamp`
    local flag=0

    if [[ -z ${timestamp_next_test} ]]; then
	setNextTestVariables ${current_timestamp}
	flag=1
    else
	isTestNeedRun ${current_timestamp}
	if [[ $? == 0 ]]; then
	    echo "-------- run test begin --------"
	    runTest
	    setNextTestVariables ${current_timestamp}
	    flag=2
	    echo "-------- run test done --------"
	fi
    fi

    if [[ $flag != 2 ]]; then
	isHealthChecked ${current_timestamp}
	if [[ $? != 0 ]]; then
	    echo "-------- check health state begin --------"
	    checkHealth
	    timestamp_last_check=${current_timestamp}
	    flag=3
	    echo "-------- check health state done --------"
	fi
    fi

    if [[ $flag != 0 ]]; then
    	saveData
	echo "-------- save data done --------"
    fi

    #read -p "<prese to continue>"
}

main
